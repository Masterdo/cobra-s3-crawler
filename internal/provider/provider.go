package provider

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/Masterdo/cobra-s3-crawler/internal/crawler"
)

// Provider is the main package struct
// TODO Not rely on this interface from crawler package
type Provider struct {
	clientList map[string]crawler.S3Lister
}

// NewProvider instanciates the empty service map
func NewProvider() (*Provider, error) {
	clientList := map[string]crawler.S3Lister{}

	provider := &Provider{
		clientList: clientList,
	}

	return provider, nil
}

// GetClientForRegion returns s3 client for required region
func (p *Provider) GetClientForRegion(region string) crawler.S3Lister {
	if client, ok := p.clientList[region]; ok {
		return client
	}
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)

	if err != nil {
		fmt.Printf("Couldn't open S3 session, %v\n", err)
		os.Exit(1)
	}

	svc := s3.New(sess)
	p.clientList[region] = svc
	return svc
}
