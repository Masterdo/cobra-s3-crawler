package config

import "os"

// Config is the public facing struct for configuration
type Config struct {
	UpdateEndpoint   string
	AWSRegion        string
	EquinoxPublicKey []byte
	EquinoxAppID     string
}

// NewConfig creates new configuration with defaults and env vars
func NewConfig() (*Config, error) {

	region := os.Getenv("AWS_REGION")
	if region == "" {
		region = "us-east-1"
	}

	c := &Config{
		UpdateEndpoint: "https://equinox.io",
		AWSRegion:      region,
		EquinoxAppID:   "app_2k4FUuCKSE6",
		EquinoxPublicKey: []byte(`
-----BEGIN ECDSA PUBLIC KEY-----
MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEgI8fPApqbGTrorCLO27WU0jSTsI7cWGA
uNj3A1tqirTdZBRQb0w6kHFcVRbTwjUxvWOC+oFLjMYiS5Se7kYWjbA10C+xhII0
oPOq5FIlsXlWPaVJX/jzwRYXjQecPVNh
-----END ECDSA PUBLIC KEY-----
`),
	}

	return c, nil
}
