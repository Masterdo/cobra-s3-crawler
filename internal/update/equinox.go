package update

import (
	"context"
	"fmt"

	"github.com/equinox-io/equinox"
)

// Equinox is the main struct of this package
type Equinox struct {
	PublicKey []byte
	AppID     string
}

// Update uses Equinox to update the binay
func (e *Equinox) Update(ctx context.Context) error {
	var opts equinox.Options
	if err := opts.SetPublicKeyPEM(e.PublicKey); err != nil {
		return err
	}

	// check for the update
	resp, err := equinox.CheckContext(ctx, e.AppID, opts)
	switch {
	case err == equinox.NotAvailableErr:
		fmt.Println("No update available, already at the latest version!")
		return nil
	case err != nil:
		fmt.Println("Update failed:", err)
		return err
	}

	// fetch the update and apply it
	err = resp.ApplyContext(ctx)
	if err != nil {
		return err
	}

	fmt.Printf("Updated to new version: %s!\n", resp.ReleaseVersion)
	return nil
}
