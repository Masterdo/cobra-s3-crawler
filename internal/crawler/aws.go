package crawler

import (
	"context"
	"fmt"
	"math"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/s3"
)

// S3Lister gives all the functions of the S3 Client used by the crawler
type S3Lister interface {
	GetBucketLocationWithContext(ctx aws.Context, input *s3.GetBucketLocationInput, opts ...request.Option) (*s3.GetBucketLocationOutput, error)
	ListBucketsWithContext(ctx aws.Context, input *s3.ListBucketsInput, opts ...request.Option) (*s3.ListBucketsOutput, error)
	ListObjectsV2PagesWithContext(ctx aws.Context, input *s3.ListObjectsV2Input, fn func(*s3.ListObjectsV2Output, bool) bool, opts ...request.Option) error
}

// S3Provider provides the right service for each region of s3
type S3Provider interface {
	GetClientForRegion(region string) S3Lister
}

// Crawler is the public facing client for this package
type Crawler struct {
	S3Client      S3Lister
	S3Provider    S3Provider
	SizeFormat    string
	Sort          bool
	StorageStats  bool
	NameFilter    string
	DefaultRegion string
	ParallelPages bool
}

// Errors are all from S3 Client, print to terminal and exit
func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}

func (c *Crawler) getBucketLocation(ctx context.Context, bucketName string) string {
	input := &s3.GetBucketLocationInput{
		Bucket: aws.String(bucketName),
	}

	result, err := c.S3Client.GetBucketLocationWithContext(ctx, input)
	if err != nil {
		// Some edge cases exist here when buckets from the bucketList return 404 (zombie buckets)
		exitErrorf("Unable to get bucket location, %v", err)
	}

	if result.LocationConstraint == nil {
		return c.DefaultRegion
	}

	return aws.StringValue(result.LocationConstraint)
}

func (c *Crawler) priceBucket(bucket *Bucket) {
	const bytesIn1TB = float64(1099511627776)
	priceSoFar := 0.0
	var priceTable regionPrice

	if table, ok := regionPrices[bucket.Location]; ok {
		priceTable = table
	} else {
		// Unknown region, no pricing information available
		bucket.TotalCost = 0.0
		return
	}

	// STANDARD
	if val, ok := bucket.SizeByStorageClass["STANDARD"]; ok {
		val := float64(val)
		if val > bytesIn1TB*500 {
			priceSoFar = priceSoFar + (bytesIn1TB * 50 * priceTable.Standard50)
			priceSoFar = priceSoFar + (bytesIn1TB * 450 * priceTable.Standard450)
			priceSoFar = priceSoFar + ((val - (bytesIn1TB * 500)) * priceTable.Standard500)
		} else if val > bytesIn1TB*50 {
			priceSoFar = priceSoFar + (bytesIn1TB * 50 * priceTable.Standard50)
			priceSoFar = priceSoFar + ((val - (bytesIn1TB * 50)) * priceTable.Standard450)
		} else {
			priceSoFar = priceSoFar + (val * priceTable.Standard50)
		}
	}
	// STANDARD_IA
	if val, ok := bucket.SizeByStorageClass["STANDARD_IA"]; ok {
		priceSoFar = priceSoFar + (float64(val) * priceTable.StandardIA)
	}
	// GLACIER
	if val, ok := bucket.SizeByStorageClass["GLACIER"]; ok {
		priceSoFar = priceSoFar + (float64(val) * priceTable.Glacier)
	}

	bucket.TotalCost = priceSoFar
}

func (c *Crawler) combineBuckets(mainBucket *Bucket, tempBucket *Bucket) {
	mainBucket.NumberOfFiles = mainBucket.NumberOfFiles + tempBucket.NumberOfFiles
	mainBucket.TotalSize = mainBucket.TotalSize + tempBucket.TotalSize
	if mainBucket.LastModified.After(tempBucket.LastModified) {
		mainBucket.LastModified = tempBucket.LastModified
	}
	if c.StorageStats {
		for class, value := range tempBucket.StorageClassStats {
			mainBucket.StorageClassStats[class] = mainBucket.StorageClassStats[class] + value
		}
	}
	for class, value := range tempBucket.SizeByStorageClass {
		mainBucket.SizeByStorageClass[class] = mainBucket.SizeByStorageClass[class] + value
	}
}

func (c *Crawler) enrichBucketPricingInformationParallel(ctx context.Context, bucket *Bucket, nChan chan<- *Bucket) {
	input := &s3.ListObjectsV2Input{
		Bucket: aws.String(bucket.Name),
	}

	pageChannel := make(chan *Bucket)

	processPage := func(page *s3.ListObjectsV2Output, bucket *Bucket, channel chan<- *Bucket) {
		for _, file := range page.Contents {
			bucket.TotalSize = bucket.TotalSize + *file.Size
			bucket.NumberOfFiles++
			if bucket.LastModified.After(aws.TimeValue(file.LastModified)) {
				bucket.LastModified = aws.TimeValue(file.LastModified)
			}
			if c.StorageStats {
				bucket.StorageClassStats[aws.StringValue(file.StorageClass)]++
			}
			bucket.SizeByStorageClass[aws.StringValue(file.StorageClass)] = bucket.SizeByStorageClass[aws.StringValue(file.StorageClass)] + *file.Size
		}
		channel <- bucket
	}

	var pages = []*s3.ListObjectsV2Output{}

	c.S3Provider.GetClientForRegion(bucket.Location).ListObjectsV2PagesWithContext(ctx, input,
		func(page *s3.ListObjectsV2Output, lastPage bool) bool {
			pages = append(pages, page)
			return !lastPage
		})

	for _, page := range pages {
		progressBucket := &Bucket{
			Name:               "temp",
			CreationDate:       time.Now(),
			LastModified:       time.Date(2034, 10, 28, 15, 30, 0, 0, time.UTC),
			StorageClassStats:  map[string]int64{},
			SizeByStorageClass: map[string]int64{},
		}
		go processPage(page, progressBucket, pageChannel)
	}

	pagesProcessed := 0
	numberOfPages := len(pages)

	done := pagesProcessed == numberOfPages

	for !done {
		select {
		case tempBucket := <-pageChannel:
			pagesProcessed++
			c.combineBuckets(bucket, tempBucket)
			done = pagesProcessed == numberOfPages
		case <-ctx.Done():
			exitErrorf("Request Cancelled")
		}
	}

	c.priceBucket(bucket)

	nChan <- bucket
	return
}

func (c *Crawler) enrichBucketPricingInformation(ctx context.Context, bucket *Bucket, nChan chan<- *Bucket) {
	input := &s3.ListObjectsV2Input{
		Bucket: aws.String(bucket.Name),
	}

	c.S3Provider.GetClientForRegion(bucket.Location).ListObjectsV2PagesWithContext(ctx, input,
		func(page *s3.ListObjectsV2Output, lastPage bool) bool {
			for _, file := range page.Contents {
				bucket.TotalSize = bucket.TotalSize + *file.Size
				bucket.NumberOfFiles++
				if bucket.LastModified.After(aws.TimeValue(file.LastModified)) {
					bucket.LastModified = aws.TimeValue(file.LastModified)
				}
				if c.StorageStats {
					bucket.StorageClassStats[aws.StringValue(file.StorageClass)]++
				}

				bucket.SizeByStorageClass[aws.StringValue(file.StorageClass)] = bucket.SizeByStorageClass[aws.StringValue(file.StorageClass)] + *file.Size

			}
			return !lastPage
		})

	c.priceBucket(bucket)

	nChan <- bucket
	return
}

func (c *Crawler) printBucket(bucket *Bucket) {
	var sizeFmt sizeFormat
	if val, ok := sizeFormats[c.SizeFormat]; ok {
		sizeFmt = val
	} else {
		sizeFmt = sizeFormats["b"]
	}

	fmt.Printf("*******************\n")
	fmt.Printf("Name: %s\n", bucket.Name)
	fmt.Printf("Location: %s\n", bucket.Location)
	fmt.Printf("CreationDate: %s\n", bucket.CreationDate)
	fmt.Printf("LastModified: %s\n", bucket.LastModified)
	fmt.Printf("Total Number of Files: %d\n", bucket.NumberOfFiles)
	for class, numberOfFiles := range bucket.StorageClassStats {
		fmt.Printf("Storage Class: %s, Files: %d\n", class, numberOfFiles)
	}
	fmt.Printf("Total Size: %.2f %s\n", float64(bucket.TotalSize)*math.Pow10(sizeFmt.SizeModifier), sizeFmt.Label)
	fmt.Printf("Total Cost: $%f\n", bucket.TotalCost)
	fmt.Printf("*******************\n")
	return
}

func (c *Crawler) enrichBucketLocations(ctx context.Context, buckets []*Bucket) {

	for _, b := range buckets {
		location := c.getBucketLocation(ctx, b.Name)
		b.Location = location
	}
}

func (c *Crawler) filterBucketList(buckets []*s3.Bucket) []*Bucket {

	resultBuckets := []*Bucket{}
	keep := func(bucket *s3.Bucket) bool {
		if c.NameFilter == "" {
			return true
		}
		return strings.Contains(aws.StringValue(bucket.Name), c.NameFilter)
	}
	for _, b := range buckets {
		if keep(b) {
			resultBuckets = append(resultBuckets, &Bucket{
				Name:               aws.StringValue(b.Name),
				CreationDate:       aws.TimeValue(b.CreationDate),
				LastModified:       time.Date(2034, 10, 28, 15, 30, 0, 0, time.UTC),
				StorageClassStats:  map[string]int64{},
				SizeByStorageClass: map[string]int64{},
			})
		}
	}
	return resultBuckets
}

func (c *Crawler) getBucketList(ctx context.Context) []*Bucket {
	result, err := c.S3Client.ListBucketsWithContext(ctx, nil)
	if err != nil {
		exitErrorf("Unable to list buckets, %v", err)
	}

	buckets := c.filterBucketList(result.Buckets)

	c.enrichBucketLocations(ctx, buckets)

	return buckets
}

func (c *Crawler) processBucketList(ctx context.Context, buckets []*Bucket, header string) {
	numberOfBuckets := len(buckets)
	var bucketsProcessed int
	fmt.Printf("%s:\n", header)

	bucketChannel := make(chan *Bucket)

	for _, b := range buckets {
		if c.ParallelPages {
			go c.enrichBucketPricingInformationParallel(ctx, b, bucketChannel)
		} else {
			go c.enrichBucketPricingInformation(ctx, b, bucketChannel)
		}
	}

	done := bucketsProcessed == numberOfBuckets

	if done {
		fmt.Println("No data found.")
	}

	for !done {
		select {
		case bucket := <-bucketChannel:
			bucketsProcessed++
			c.printBucket(bucket)
			done = bucketsProcessed == numberOfBuckets
		case <-ctx.Done():
			exitErrorf("Request Cancelled")
		}
	}
}

func (c *Crawler) pivotByRegion(buckets []*Bucket) map[string][]*Bucket {
	result := make(map[string][]*Bucket)

	if c.Sort {
		for _, bucket := range buckets {
			result[bucket.Location] = append(result[bucket.Location], bucket)
		}
	} else {
		result["Buckets"] = buckets
	}
	return result
}

// ListBuckets is the entry point. It will list and print information about buckets.
func (c *Crawler) ListBuckets(ctx context.Context) {

	buckets := c.getBucketList(ctx)

	sortedBuckets := c.pivotByRegion(buckets)

	for header, bucketList := range sortedBuckets {
		c.processBucketList(ctx, bucketList, header)
	}
}
