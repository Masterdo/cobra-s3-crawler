package crawler

import (
	"context"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/stretchr/testify/assert"
)

type MockS3Lister struct {
	Region            string
	RegionError       error
	Buckets           []*s3.Bucket
	ListBucketsError  error
	ListObjectsOutput *s3.ListObjectsV2Output
	ListObjectsError  error
}

type MockS3Provider struct {
	Lister S3Lister
}

func (m *MockS3Provider) GetClientForRegion(region string) S3Lister {
	return m.Lister
}

func (m *MockS3Lister) GetBucketLocationWithContext(ctx aws.Context, input *s3.GetBucketLocationInput, opts ...request.Option) (*s3.GetBucketLocationOutput, error) {
	return &s3.GetBucketLocationOutput{LocationConstraint: aws.String(m.Region)}, m.RegionError
}

func (m *MockS3Lister) ListBucketsWithContext(ctx aws.Context, input *s3.ListBucketsInput, opts ...request.Option) (*s3.ListBucketsOutput, error) {
	return &s3.ListBucketsOutput{Buckets: m.Buckets, Owner: nil}, m.ListBucketsError
}

func (m *MockS3Lister) ListObjectsV2PagesWithContext(ctx aws.Context, input *s3.ListObjectsV2Input, fn func(*s3.ListObjectsV2Output, bool) bool, opts ...request.Option) error {
	fn(m.ListObjectsOutput, true)
	return m.ListObjectsError
}

func TestPriceBucket(t *testing.T) {
	// Given
	testCrawler := &Crawler{}

	bucket := &Bucket{
		Location: "us-east-2",
		SizeByStorageClass: map[string]int64{
			"STANDARD":    5000000000,
			"STANDARD_IA": 50000000000,
			"GLACIER":     5000000000000,
		},
	}

	// When
	testCrawler.priceBucket(bucket)

	// Then
	assert.Equal(t, 20.74, bucket.TotalCost)
}

func TestPriceBucketWrongRegion(t *testing.T) {
	// Given
	testCrawler := &Crawler{}

	bucket := &Bucket{
		Location: "us-east-42",
		SizeByStorageClass: map[string]int64{
			"STANDARD":    5000000000,
			"STANDARD_IA": 50000000000,
			"GLACIER":     5000000000000,
		},
	}

	// When
	testCrawler.priceBucket(bucket)

	// Then
	assert.Equal(t, 0.0, bucket.TotalCost)
}

func TestGetBucketLocation(t *testing.T) {
	// Given
	mockLister := &MockS3Lister{
		Region: "us-east-1",
	}
	testCrawler := &Crawler{
		S3Client: mockLister,
	}

	// When
	region := testCrawler.getBucketLocation(context.Background(), "testName")

	// Then
	assert.Equal(t, "us-east-1", region)
}

func TestEnrichBucketPricingInformation(t *testing.T) {
	// Given
	laterObject := time.Date(2000, 2, 1, 12, 30, 0, 0, time.UTC)
	earlyObject := time.Date(1999, 2, 1, 12, 30, 0, 0, time.UTC)
	outputs := &s3.ListObjectsV2Output{Contents: []*s3.Object{
		{Size: aws.Int64(50000000), LastModified: aws.Time(laterObject), StorageClass: aws.String("STANDARD")},
		{Size: aws.Int64(50000000), LastModified: aws.Time(earlyObject), StorageClass: aws.String("GLACIER")},
		{Size: aws.Int64(50000000), LastModified: aws.Time(laterObject), StorageClass: aws.String("GLACIER")},
	}}

	mockLister := &MockS3Lister{
		ListObjectsOutput: outputs,
	}

	mockProvider := &MockS3Provider{
		Lister: mockLister,
	}

	testCrawler := &Crawler{
		S3Client:     mockLister,
		S3Provider:   mockProvider,
		StorageStats: true,
	}

	bucket := &Bucket{
		Name:         "test-name",
		CreationDate: laterObject,
		Location:     "us-east-2",
		// TODO Solve the mystery of why a maximum date fails here
		// Example: time.Unix(1<<63-1, 999999999),
		LastModified:       time.Date(2034, 10, 28, 15, 30, 0, 0, time.UTC),
		StorageClassStats:  map[string]int64{},
		SizeByStorageClass: map[string]int64{},
	}

	nChan := make(chan *Bucket)

	// When
	go testCrawler.enrichBucketPricingInformation(context.Background(), bucket, nChan)

	processedBucket := <-nChan

	// Then
	assert.Equal(t, int64(150000000), processedBucket.TotalSize)
	assert.Equal(t, earlyObject, processedBucket.LastModified)
	assert.Equal(t, float64(0.00155), processedBucket.TotalCost)
}

func TestEnrichBucketPricingInformationParallel(t *testing.T) {
	// Given
	laterObject := time.Date(2000, 2, 1, 12, 30, 0, 0, time.UTC)
	earlyObject := time.Date(1999, 2, 1, 12, 30, 0, 0, time.UTC)
	outputs := &s3.ListObjectsV2Output{Contents: []*s3.Object{
		{Size: aws.Int64(50000000), LastModified: aws.Time(laterObject), StorageClass: aws.String("STANDARD")},
		{Size: aws.Int64(50000000), LastModified: aws.Time(earlyObject), StorageClass: aws.String("GLACIER")},
		{Size: aws.Int64(50000000), LastModified: aws.Time(laterObject), StorageClass: aws.String("GLACIER")},
	}}

	mockLister := &MockS3Lister{
		ListObjectsOutput: outputs,
	}

	mockProvider := &MockS3Provider{
		Lister: mockLister,
	}

	testCrawler := &Crawler{
		S3Client:     mockLister,
		S3Provider:   mockProvider,
		StorageStats: true,
	}

	bucket := &Bucket{
		Name:         "test-name",
		CreationDate: laterObject,
		Location:     "us-east-2",
		// TODO Solve the mystery of why a maximum date fails here
		// Example: time.Unix(1<<63-1, 999999999),
		LastModified:       time.Date(2034, 10, 28, 15, 30, 0, 0, time.UTC),
		StorageClassStats:  map[string]int64{},
		SizeByStorageClass: map[string]int64{},
	}

	nChan := make(chan *Bucket)

	// When
	go testCrawler.enrichBucketPricingInformationParallel(context.Background(), bucket, nChan)

	processedBucket := <-nChan

	// Then
	assert.Equal(t, int64(150000000), processedBucket.TotalSize)
	assert.Equal(t, earlyObject, processedBucket.LastModified)
	assert.Equal(t, float64(0.00155), processedBucket.TotalCost)
}

func TestPrintBucketNoOptions(t *testing.T) {
	// Given
	testCrawler := &Crawler{
		SizeFormat: "b",
	}
	bucket := &Bucket{
		Name:     "test-name",
		Location: "us-east-2",
	}
	// When
	testCrawler.printBucket(bucket)

	// Then
	// No panic
}

func TestPrintBucketWithOptions(t *testing.T) {
	// Given
	testCrawler := &Crawler{
		SizeFormat:   "b",
		StorageStats: true,
	}
	bucket := &Bucket{
		Name:               "test-name",
		Location:           "us-east-2",
		StorageClassStats:  map[string]int64{"STANDARD": int64(45), "GLACIER": int64(5000)},
		SizeByStorageClass: map[string]int64{"STANDARD": int64(500000), "GLACIER": int64(5000000)},
	}
	// When
	testCrawler.printBucket(bucket)

	// Then
	// No panic
}

func TestEnrichBucketLocations(t *testing.T) {
	// Given
	mockLister := &MockS3Lister{
		Region: "us-east-1",
	}
	testCrawler := &Crawler{
		S3Client:     mockLister,
		SizeFormat:   "b",
		StorageStats: true,
	}

	buckets := []*Bucket{{Name: "test-1"}, {Name: "test-2"}}

	// When

	testCrawler.enrichBucketLocations(context.Background(), buckets)

	// Then
	assert.Equal(t, "us-east-1", buckets[0].Location)
	assert.Equal(t, "us-east-1", buckets[1].Location)
}

func TestFilterBucketList(t *testing.T) {
	// Given
	testCrawler := &Crawler{}

	buckets := []*s3.Bucket{
		{Name: aws.String("test-name")},
		{Name: aws.String("test-name-2")},
		{Name: aws.String("test-noname-3")},
	}

	// When

	filteredBuckets := testCrawler.filterBucketList(buckets)

	// Then
	assert.Equal(t, 3, len(filteredBuckets))
}

func TestFilterBucketListWithFilter(t *testing.T) {
	// Given
	testCrawler := &Crawler{
		NameFilter: "test-name",
	}

	buckets := []*s3.Bucket{
		{Name: aws.String("test-name")},
		{Name: aws.String("test-name-2")},
		{Name: aws.String("test-noname-3")},
	}

	// When

	filteredBuckets := testCrawler.filterBucketList(buckets)

	// Then
	assert.Equal(t, 2, len(filteredBuckets))
}

func TestGetBucketList(t *testing.T) {
	// Given
	buckets := []*s3.Bucket{
		{Name: aws.String("test-name")},
		{Name: aws.String("test-name-2")},
		{Name: aws.String("test-noname-3")},
	}

	mockLister := &MockS3Lister{
		Buckets: buckets,
	}
	testCrawler := &Crawler{
		S3Client: mockLister,
	}

	// When
	resultBuckets := testCrawler.getBucketList(context.Background())

	// Then
	assert.Equal(t, 3, len(resultBuckets))
}

func TestProcessBucketList(t *testing.T) {
	//Given
	lastModified := time.Date(2000, 2, 1, 12, 30, 0, 0, time.UTC)
	bucket1 := &Bucket{
		Name:               "test-name",
		CreationDate:       lastModified,
		Location:           "us-east-2",
		StorageClassStats:  map[string]int64{},
		SizeByStorageClass: map[string]int64{},
	}
	bucket2 := &Bucket{
		Name:               "test-name2",
		CreationDate:       lastModified,
		Location:           "us-east-2",
		StorageClassStats:  map[string]int64{},
		SizeByStorageClass: map[string]int64{},
	}
	buckets := []*Bucket{bucket1, bucket2}

	outputs := &s3.ListObjectsV2Output{Contents: []*s3.Object{
		{Size: aws.Int64(50000000), LastModified: aws.Time(lastModified), StorageClass: aws.String("STANDARD")},
		{Size: aws.Int64(50000000), LastModified: aws.Time(lastModified), StorageClass: aws.String("GLACIER")},
		{Size: aws.Int64(50000000), LastModified: aws.Time(lastModified), StorageClass: aws.String("GLACIER")},
	}}

	mockLister := &MockS3Lister{
		ListObjectsOutput: outputs,
	}

	mockProvider := &MockS3Provider{
		Lister: mockLister,
	}

	testCrawler := &Crawler{
		S3Client:   mockLister,
		S3Provider: mockProvider,
	}

	// When
	testCrawler.processBucketList(context.Background(), buckets, "Test")

	//Then
	// No panic
}

func TestProcessBucketListNoData(t *testing.T) {
	//Given

	buckets := []*Bucket{}

	testCrawler := &Crawler{}

	// When
	testCrawler.processBucketList(context.Background(), buckets, "Test")

	//Then
	// No panic
}

func TestProcessBucketListCancelled(t *testing.T) {
	//Given

	buckets := []*Bucket{}

	testCrawler := &Crawler{}

	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	// When
	testCrawler.processBucketList(ctx, buckets, "Test")

	//Then
	// No panic
}

func TestPivotByRegionNoSort(t *testing.T) {
	// Given
	testCrawler := &Crawler{
		Sort: false,
	}
	bucket1 := &Bucket{
		Name:     "test-name",
		Location: "us-east-1",
	}
	bucket2 := &Bucket{
		Name:     "test-name2",
		Location: "us-east-2",
	}
	buckets := []*Bucket{bucket1, bucket2}

	// When
	resultMap := testCrawler.pivotByRegion(buckets)

	// Then
	assert.Equal(t, 2, len(resultMap["Buckets"]))

}

func TestPivotByRegionSort(t *testing.T) {
	// Given
	testCrawler := &Crawler{
		Sort: true,
	}
	bucket1 := &Bucket{
		Name:     "test-name",
		Location: "us-east-1",
	}
	bucket2 := &Bucket{
		Name:     "test-name2",
		Location: "us-east-2",
	}
	buckets := []*Bucket{bucket1, bucket2}

	// When
	resultMap := testCrawler.pivotByRegion(buckets)

	// Then
	assert.Equal(t, 1, len(resultMap["us-east-1"]))
	assert.Equal(t, 1, len(resultMap["us-east-2"]))
}

func TestCombineBuckets(t *testing.T) {
	// Given
	early := time.Date(1999, 2, 1, 12, 30, 0, 0, time.UTC)
	late := time.Date(2000, 2, 1, 12, 30, 0, 0, time.UTC)
	mainBucket := &Bucket{
		Name:               "test-name",
		LastModified:       late,
		Location:           "us-east-2",
		TotalSize:          int64(100),
		NumberOfFiles:      int64(50),
		StorageClassStats:  map[string]int64{"STANDARD": int64(45), "GLACIER": int64(5000)},
		SizeByStorageClass: map[string]int64{"STANDARD": int64(500000), "GLACIER": int64(5000000)},
	}
	tempBucket := &Bucket{
		Name:               "test-name2",
		LastModified:       early,
		Location:           "us-east-3",
		TotalSize:          int64(200),
		NumberOfFiles:      int64(50),
		StorageClassStats:  map[string]int64{"STANDARD": int64(45), "GLACIER": int64(5000)},
		SizeByStorageClass: map[string]int64{"STANDARD": int64(500000), "GLACIER": int64(5000000)},
	}

	testCrawler := &Crawler{
		StorageStats: true,
	}

	// When
	testCrawler.combineBuckets(mainBucket, tempBucket)

	// Then
	assert.Equal(t, early, mainBucket.LastModified)
	assert.Equal(t, "test-name", mainBucket.Name)
	assert.Equal(t, "us-east-2", mainBucket.Location)
	assert.Equal(t, int64(300), mainBucket.TotalSize)
	assert.Equal(t, int64(100), mainBucket.NumberOfFiles)
	assert.Equal(t, int64(90), mainBucket.StorageClassStats["STANDARD"])
	assert.Equal(t, int64(10000), mainBucket.StorageClassStats["GLACIER"])
	assert.Equal(t, int64(1000000), mainBucket.SizeByStorageClass["STANDARD"])
	assert.Equal(t, int64(10000000), mainBucket.SizeByStorageClass["GLACIER"])
}
