package crawler

import "time"

// Bucket lists all characteristics of a s3 bucket we are interested in
type Bucket struct {
	Name               string
	CreationDate       time.Time
	Location           string
	NumberOfFiles      int64
	TotalSize          int64
	SizeByStorageClass map[string]int64
	TotalCost          float64
	LastModified       time.Time
	StorageClassStats  map[string]int64
}

type sizeFormat struct {
	Label        string
	SizeModifier int
}

var sizeFormats = map[string]sizeFormat{
	"b":  sizeFormat{Label: "bytes", SizeModifier: 0},
	"kb": sizeFormat{Label: "KB", SizeModifier: -3},
	"mb": sizeFormat{Label: "MB", SizeModifier: -6},
	"gb": sizeFormat{Label: "GB", SizeModifier: -9}}
