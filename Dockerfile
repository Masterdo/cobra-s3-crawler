FROM golang:1.13-stretch as cache

LABEL maintainer=masterdomagic@gmail.com \
      repo=gitlab.com/Masterdo/cobra-s3-crawler

WORKDIR /app

# Do NOT download all the dependencies on every build!
COPY go.mod go.sum ./

RUN go mod download

COPY cmd cmd

COPY internal internal

ENV CGO_ENABLED=0
ENV GOARCH=amd64
ARG goos
ENV GOOS=${goos}

RUN go build -o cobra-s3-crawler.${GOOS} ./cmd/...
