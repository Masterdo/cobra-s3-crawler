#!/bin/bash

set -euo pipefail

GOOS=${GOOS:-darwin}

mkdir $PWD/bin 2> /dev/null || true

docker build -t cobra-s3-crawler-${GOOS}  --build-arg goos=${GOOS} . >&2

USERID=${UID:-1000}

docker run --entrypoint=sh --user=$USERID --rm -v $PWD/bin:/home_bin cobra-s3-crawler-${GOOS} -c 'cp cobra-s3-crawler* /home_bin'
