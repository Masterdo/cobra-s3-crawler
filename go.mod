module gitlab.com/Masterdo/cobra-s3-crawler

go 1.13

require (
	github.com/aws/aws-sdk-go v1.25.41
	github.com/equinox-io/equinox v1.2.0
	github.com/magiconair/properties v1.8.1
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.5.0
	github.com/stretchr/testify v1.2.2
)
