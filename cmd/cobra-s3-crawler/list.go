package main

import (
	"context"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/spf13/cobra"
	"gitlab.com/Masterdo/cobra-s3-crawler/internal/config"
	"gitlab.com/Masterdo/cobra-s3-crawler/internal/crawler"
	"gitlab.com/Masterdo/cobra-s3-crawler/internal/provider"
)

// ListCommand is the top level command for listing in this CLI
func ListCommand(ctx context.Context, cfg *config.Config) *cobra.Command {

	var sizeFormat string
	var nameFilter string
	var sort bool
	var storageStats bool
	var parallelPages bool

	var cmdList = &cobra.Command{
		Use:   "list",
		Short: "List Buckets with info on files and pricing",
		Long:  "List Buckets with info on files and pricing",
		Run: func(cmd *cobra.Command, args []string) {

			sess, err := session.NewSession(&aws.Config{
				Region: aws.String(cfg.AWSRegion)},
			)

			if err != nil {
				fmt.Printf("Couldn't open S3 session, %v\n", err)
				os.Exit(1)
			}

			svc := s3.New(sess)

			s3Provider, _ := provider.NewProvider()

			s3Crawler := &crawler.Crawler{
				S3Client:      svc,
				S3Provider:    s3Provider,
				SizeFormat:    sizeFormat,
				Sort:          sort,
				StorageStats:  storageStats,
				NameFilter:    nameFilter,
				DefaultRegion: cfg.AWSRegion,
				ParallelPages: parallelPages,
			}

			ctx, cancel := context.WithCancel(ctx)
			defer cancel()

			s3Crawler.ListBuckets(ctx)
		},
	}

	cmdList.Flags().StringVarP(&sizeFormat, "format-size", "f", "b", "size display format (b, kb, mb, gb). Defaults to bytes.")
	cmdList.Flags().StringVarP(&nameFilter, "filter-name", "n", "", "Display only buckets containing this string")
	cmdList.Flags().BoolVar(&sort, "sort-by-region", false, "flag to sort bucket list by region")
	cmdList.Flags().BoolVar(&parallelPages, "process-parallel-pages", true, "experimental flag to process pages in parallel")
	cmdList.Flags().BoolVar(&storageStats, "storage-stats", false, "flag to display storage class statistics")

	return cmdList
}
