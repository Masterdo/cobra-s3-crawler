package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/Masterdo/cobra-s3-crawler/internal/config"
)

func main() {

	config, err := config.NewConfig()

	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// This command might take a while, we want the user to be able to fully cancel it
	interuptSignal := make(chan os.Signal, 1)
	signal.Notify(interuptSignal, os.Interrupt, syscall.SIGTERM)
	go func() {
		defer cancel()
		fmt.Printf("Received %s signal\n", <-interuptSignal)
	}()
	// Calls the main command passing the context, for cancelling purposes
	Execute(ctx, config)
}
