package main

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Masterdo/cobra-s3-crawler/internal/config"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "csc",
	Short: "CLI to crawl and get pricing information on s3 buckets",
	Long: `CLI Tool to list information on s3 buckets.
	Will display size, number of files and pricing information for buckets.
	Can list by regions, supports various formats for size and can filter bucket names or storage class.
	This is the updated version!!`,
	// Don't let cobra output "Error: ..." when there's an error with a command
	// since we are doing this in the Execute function below
	SilenceErrors: true,
	// Don't output usage when a command returns an error, it will still output
	// usage if you type a command or option that doesn't exist
	SilenceUsage: true,
}

// Execute is where we add subcommands to the main command defined above and execute them
func Execute(ctx context.Context, cfg *config.Config) {

	rootCmd.AddCommand(ListCommand(ctx, cfg))
	rootCmd.AddCommand(UpdateCommand(ctx, cfg))

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

func initConfig() {
	viper.AutomaticEnv()
}
