package main

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/Masterdo/cobra-s3-crawler/internal/config"
	"gitlab.com/Masterdo/cobra-s3-crawler/internal/update"
)

// UpdateCommand uses equinox to update the binary
func UpdateCommand(ctx context.Context, cfg *config.Config) *cobra.Command {

	var cmdUpdate = &cobra.Command{
		Use:   "update",
		Short: "Update the tool using Equinox",
		Long:  "Update the tool using Equinox",
		RunE: func(cmd *cobra.Command, args []string) error {

			equinox := &update.Equinox{
				PublicKey: cfg.EquinoxPublicKey,
				AppID:     cfg.EquinoxAppID,
			}
			return equinox.Update(ctx)
		},
	}

	return cmdUpdate
}
