# cobra-s3-crawler

This golang cobra CLI tool will peruse your s3 storage and give some information on how much it is costing. 

## Installation

This tool is distributed using Equinox.io. The download page for it can be found here: https://dl.equinox.io/masterdo/cobra-s3-crawler/stable

Follow the instructions for your OS. 

## AWS Credentials

To use the tool, you must provide valid AWS credentials, for a user with access to the buckets you want to inspect.

You can set the credentials using either Environment Variables, the shared credentials file or pass them directly to each command.

### Linux, OS X, or Unix Env Vars
```
$ export AWS_ACCESS_KEY_ID=YOUR_AKID
$ export AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY
$ export AWS_REGION=default-region
```

### Windows Env Vars
```
> set AWS_ACCESS_KEY_ID=YOUR_AKID
> set AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY
> set AWS_REGION=default-region
```

### Credentials file
```
[some-profile-name]
aws_access_key_id = <YOUR_DEFAULT_ACCESS_KEY_ID>
aws_secret_access_key = <YOUR_DEFAULT_SECRET_ACCESS_KEY>
``` 
This file is located in `~/.aws/credentials`. You then need to expose the environment variable `AWS_PROFILE` with the profile name as the value, or use `default` as the profile name. You still need to expose `AWS_REGION` or the default `us-east-1` will be used.

### Passing values directly to the command
You can define, or override environment variables when calling the commands. This is not recommended for sensitive credentials since they appear in terminal history. You could however safely set the `AWS_PROFILE` and `AWS_REGION` variables.
```
AWS_PROFILE=some-profile-name AWS_REGION=us-east-1 cobra-s3-crawler list -f mb
```
## Update

This tool uses go-update to check and apply available updates to itself. It uses the hosted service from Equinox.io to do so. To run the update, use `cobra-s3-crawler update`. To see this working it is best to start with a version that is not the latest. Anything 1.0.2 and above has the proper update plumbing. The main usage display (simply calling `cobra-s3-crawler`) should give visual indication that the version changed. 