#!/bin/bash

set -euo pipefail

for GOOS in darwin linux windows; do
  for GOARCH in amd64; do
    echo "Building $GOOS-$GOARCH"
    export GOOS=$GOOS
    export GOARCH=$GOARCH
    ./build.sh
  done
done